package com.gforce.gforce.user;

import java.net.URL;

public class User {
    URL qrUrl;
    String firstName;
    String lastName;


    public URL getQrUrl() {
        return qrUrl;
    }

    public void setQrUrl(URL qrUrl) {
        this.qrUrl = qrUrl;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
