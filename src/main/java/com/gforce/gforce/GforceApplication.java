package com.gforce.gforce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GforceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GforceApplication.class, args);
	}

}
