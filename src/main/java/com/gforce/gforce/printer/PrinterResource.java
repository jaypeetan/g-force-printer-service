package com.gforce.gforce.printer;

import com.gforce.gforce.qr.Qr;
import com.gforce.gforce.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.io.IOException;

@RestController
@RequestMapping("/print")
public class PrinterResource {

    @Autowired
    private PrinterService printerService;

    @CrossOrigin
    @RequestMapping(path="/printStudentStub", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public synchronized void printQRCodeWithURL(@RequestBody User user) {
        printerService.printStudentQRCode(user);
    }

    @ResponseBody
    @RequestMapping(path="/printQRCode", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public synchronized void printQrCode(@RequestBody Qr qr) throws IOException {
        printerService.printClassBookingStub(qr);
    }
}
