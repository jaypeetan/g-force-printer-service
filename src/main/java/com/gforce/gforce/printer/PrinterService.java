package com.gforce.gforce.printer;

import com.gforce.gforce.qr.Qr;
import com.gforce.gforce.user.User;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import javax.print.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.print.*;
import java.io.*;
import java.util.Base64;

@Service("printerService")
public class PrinterService implements Printable {
    BufferedImage image;
    String userText = "";

    public void printClassBookingStub(Qr qr) throws IOException {
        String printerName = "HP DeskJet 5820 series (Network)";
        try {
            Base64.Decoder decoder = Base64.getDecoder();
            byte[] decodedString = decoder.decode((qr.getBase64().split(",")[1]));
            image = ImageIO.read(new ByteArrayInputStream(decodedString));
            PrintService printService[] = PrintServiceLookup.lookupPrintServices(DocFlavor.BYTE_ARRAY.AUTOSENSE, null);
            PrintService service = findPrintService(printerName, printService);
            printImage(service);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void printStudentQRCode(User user) {
        String printerName = "58 Printer";
        PrintService printService[] = PrintServiceLookup.lookupPrintServices(DocFlavor.BYTE_ARRAY.AUTOSENSE, null);
        PrintService service = findPrintService(printerName, printService);

        try {
            image = ImageIO.read(user.getQrUrl());
            printImage(service);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printImage(PrintService service){
        try {
            PrinterJob pj = PrinterJob.getPrinterJob();
            pj.setPrintService(service);
            PageFormat pf = pj.defaultPage();
            Paper paper = pf.getPaper();
            double width = fromCMToPPI(5); //old value 8.6
            double height = fromCMToPPI(5); //old value 5.4
            paper.setSize(width, height);
            paper.setImageableArea(
                    fromCMToPPI(0.1),
                    fromCMToPPI(0.1),
                    width - fromCMToPPI(0.1),
                    height - fromCMToPPI(0.1));
            pf.setOrientation(PageFormat.PORTRAIT);
            pf.setPaper(paper);
            PageFormat validatePage = pj.validatePage(pf);
            System.out.println("Valid- " + dump(validatePage));
            pj.setPrintable(this, validatePage);
            try {
                pj.print();
            } catch (PrinterException ex) {
                ex.printStackTrace();
            }

        } catch (PrinterException e) {
            e.printStackTrace();
        }
    }

    private PrintService findPrintService(String printerName,
                                          PrintService[] services) {
        for (PrintService service : services) {
            if (service.getName().equalsIgnoreCase(printerName)) {
                return service;
            }
        }
        return null;
    }

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) {
        int result = NO_SUCH_PAGE;
        if (pageIndex < 1) {
            Graphics2D g2d = (Graphics2D) graphics;
            System.out.println("[Print] " + dump(pageFormat));
            double width = pageFormat.getImageableWidth();
            double height = pageFormat.getImageableHeight();
            System.out.println("Print Size = " + fromPPItoCM(width) + "x" + fromPPItoCM(height));
            g2d.translate((int) pageFormat.getImageableX(),
                    (int) pageFormat.getImageableY());
            Image scaled = null;
            if (width > height) {
                scaled = image.getScaledInstance((int)Math.round(width), -1, Image.SCALE_SMOOTH);
            } else {
                scaled = image.getScaledInstance(-1, (int)Math.round(height), Image.SCALE_SMOOTH);
            }
            g2d.drawImage(scaled, 0, 0, null);
            g2d.drawString(userText, 30,136);
            result = PAGE_EXISTS;
        }
        return result;
    }

    protected static double fromPPItoCM(double dpi) {
        return dpi / 72 / 0.393700787;
    }

    protected static double fromCMToPPI(double cm) {
        return toPPI(cm * 0.393700787);
    }

    protected static double toPPI(double inch) {
        return inch * 72d;
    }

    protected static String dump(Paper paper) {
        StringBuilder sb = new StringBuilder(64);
        sb.append(paper.getWidth()).append("x").append(paper.getHeight())
                .append("/").append(paper.getImageableX()).append("x").
                append(paper.getImageableY()).append(" - ").append(paper
                .getImageableWidth()).append("x").append(paper.getImageableHeight());
        return sb.toString();
    }

    protected static String dump(PageFormat pf) {
        Paper paper = pf.getPaper();
        return dump(paper);
    }

}
